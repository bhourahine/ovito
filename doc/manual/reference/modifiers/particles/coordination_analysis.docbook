<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="particles.modifiers.coordination_analysis"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Coordination analysis</title>

  <para>
    <informalfigure><screenshot><mediaobject><imageobject>
       <imagedata fileref="images/modifiers/coordination_analysis_panel.png" format="PNG" scale="50" />
    </imageobject></mediaobject></screenshot></informalfigure>
    This modifier performs two types of computations:
    <itemizedlist>
      <listitem>
        <para>
          It counts the number of neighbors for each particle that are within 
          a given cutoff range around its position. This information, the so-called <emphasis>coordination number</emphasis> of the particle, 
          will be stored in the <literal>Coordination</literal> output property by the modifier, where is available
          for subsequent operations, for example selecting particles that have a certain number of neighbors using
          the <link linkend="particles.modifiers.expression_select">Expression selection</link> modifier
          or visualizing the coordination numbers using the 
          <link linkend="particles.modifiers.color_coding">Color coding</link> modifier.
        </para>
      </listitem>
      <listitem>
        <para>
          In addition, the modifier computes the radial pair distribution function (radial PDF, or simply RDF) for the particle system as a whole.
          The radial pair distribution function <emphasis>g</emphasis>(<emphasis>r</emphasis>) measures the probability of finding a particle at distance <emphasis>r</emphasis> 
          given that there is a particle at position <emphasis>r</emphasis>=0; it is essentially a histogram of inter-particle distances. The pair distribution function is 
          normalized by the average number density of particles (i.e. the total number of particles in the simulation cell divided by its volume).
          See the <link xlink:href="https://en.wikipedia.org/wiki/Radial_distribution_function">Wikipedia</link> for more information on this distribution function.
        </para>
      </listitem>
    </itemizedlist>        
  </para>

  <simplesect>
    <title>Element-wise RDFs</title>
      <para>
        <informalfigure><screenshot><mediaobject><imageobject>
          <imagedata fileref="images/modifiers/coordination_analysis_partial_rdf.png" format="PNG" scale="50" />
        </imageobject></mediaobject></screenshot></informalfigure>
        The <emphasis>Compute partial RDFs</emphasis> option lets the modifier compute separate radial distribution functions
        for all pair-wise combinations of particle types or elements. The computed partial RDFs will be normalized such that the
        summation of the partial RDFs, weighted by the product of the two corresponding elemental concentrations, yields the total
        RDF. For example, for a binary system with two particle species <mathphrase>&#x0251;</mathphrase> and <mathphrase>&#x03B2;</mathphrase>,
        the modifier computes a set of three partials functions g<subscript>&#x0251;&#x0251;</subscript>, g<subscript>&#x0251;&#x03B2;</subscript>
        and g<subscript>&#x03B2;&#x03B2;</subscript>, which add up to the total distribution as follows:
    </para>
    <para>
      g(r) = c<subscript>&#x0251;</subscript><superscript>2</superscript> g<subscript>&#x0251;&#x0251;</subscript>(r) + 
        2 c<subscript>&#x0251;</subscript>c<subscript>&#x03B2;</subscript> g<subscript>&#x0251;&#x03B2;</subscript>(r) + 
        c<subscript>&#x03B2;</subscript><superscript>2</superscript> g<subscript>&#x03B2;&#x03B2;</subscript>(r)
    </para>
    <para>
       Here, the c<subscript>&#x0251;</subscript> and c<subscript>&#x03B2;</subscript> denote the concentrations of the two
       species in the system and the factor 2 in the mixed term appears due to g<subscript>&#x0251;&#x03B2;</subscript>(r) and
       g<subscript>&#x03B2;&#x0251;</subscript>(r) being always identical.
    </para>
  </simplesect>

  <simplesect>
    <title>Time-averaged RDF</title>
    <para>
      In the current program version, the <emphasis>Coordination analysis</emphasis> modifier can only calculate the instantaneous RDF for the current
      simulation frame. To calculate an average RDF over all frames of a simulation trajectory, you can use OVITO's
      scripting capabilities and let the <emphasis>Coordination analysis</emphasis> calculate the RDF for each frame and then average
      the histogram data over all frames with a few lines of Python code.
      See the <link xlink:href="python/modules/ovito_modifiers.html#ovito.modifiers.CoordinationAnalysisModifier">corresponding section</link> 
      of the scripting manual for an example.
    </para>
  </simplesect>

  <simplesect>
    <title>See also</title>
    <para>
      <link xlink:href="python/modules/ovito_modifiers.html#ovito.modifiers.CoordinationAnalysisModifier"><classname>CoordinationAnalysisModifier</classname> (Python API)</link>
    </para>
  </simplesect>  

</section>
