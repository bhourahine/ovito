from ovito.modifiers import CalculateDisplacementsModifier

############## Snippet code starts here ###################
modifier = CalculateDisplacementsModifier()
modifier.vis.enabled = True
modifier.vis.color = (0.8, 0.0, 0.5)